﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExamenEstebanP3.ENTITIES;
using Npgsql;

namespace ExamenEstebanP3.DAL
{
    public class AppDAL
    {
        /// <summary>
        /// inserta los datos
        /// </summary>
        /// <param name="tipos"></param>
        public void Insertar(List<EBCCR.Datos_de_INGC011_CAT_INDICADORECONOMIC> tipos)
        {
            foreach (var i in tipos)
            {
                int cod = Convert.ToInt32(i.INGC011_CAT_INDICADORECONOMIC.COD_INDICADORINTERNO.ToString());
                DateTime fec = i.INGC011_CAT_INDICADORECONOMIC.DES_FECHA;
                double val = Convert.ToDouble(i.INGC011_CAT_INDICADORECONOMIC.NUM_VALOR);

                try
                {
                    string sql = "INSERT INTO tipo_cambio.archivo( cod, fecha, valor) VALUES(@cod, @fec, @val)";

                    using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                    {
                        con.Open();
                        NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                        cmd.Parameters.AddWithValue("@cod", cod);
                        cmd.Parameters.AddWithValue("@fec", fec);
                        cmd.Parameters.AddWithValue("@val", val);
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        /// <summary>
        /// trae datos
        /// </summary>
        /// <returns></returns>
        public List<EBCCR.Datos_de_INGC011_CAT_INDICADORECONOMIC> TraerIndicadores()
        {
            List<EBCCR.Datos_de_INGC011_CAT_INDICADORECONOMIC> tipos = new List<EBCCR.Datos_de_INGC011_CAT_INDICADORECONOMIC>();
            try
            {
                
                string sql = "SELECT cod, fecha, valor FROM tipo_cambio.archivo; ";

                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {
                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    NpgsqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        tipos.Add(CargarTipos(reader));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return tipos;
        }

        /// <summary>
        /// convierte datos
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        private EBCCR.Datos_de_INGC011_CAT_INDICADORECONOMIC CargarTipos(NpgsqlDataReader reader)
        {
            EBCCR.Datos_de_INGC011_CAT_INDICADORECONOMIC tipo = new EBCCR.Datos_de_INGC011_CAT_INDICADORECONOMIC();
            tipo.INGC011_CAT_INDICADORECONOMIC.COD_INDICADORINTERNO = reader.GetInt32(0) ;
            tipo.INGC011_CAT_INDICADORECONOMIC.DES_FECHA = reader.GetDateTime(1);
            tipo.INGC011_CAT_INDICADORECONOMIC.NUM_VALOR = reader.GetDecimal(2);
            return tipo;
        }
    }
}
