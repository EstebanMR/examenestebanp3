﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExamenEstebanP3.BOL;
using ExamenEstebanP3.ENTITIES;

namespace ExamenEstebanP3
{
    public partial class Form1 : Form
    {
        AppBOL log = new AppBOL();
        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// trae los datos y los pinta
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                List<EBCCR.Datos_de_INGC011_CAT_INDICADORECONOMIC> tipos = log.CargarTipoCambio();
                List<string> fechas = new List<string>();
                List<double> valores = new List<double>();
                foreach (var i in tipos)
                {
                    string fecha = i.INGC011_CAT_INDICADORECONOMIC.DES_FECHA.ToString("MM/dd");
                    fechas.Add(fecha);
                    valores.Add(Convert.ToDouble(i.INGC011_CAT_INDICADORECONOMIC.NUM_VALOR));
                }
                Console.WriteLine(fechas.Count());
                chart1.Series[0].Points.DataBindXY(fechas, valores);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Se ha producido un error la base de datos del banco central");
                Console.WriteLine(ex.Message);
            }
        }
    }
}
