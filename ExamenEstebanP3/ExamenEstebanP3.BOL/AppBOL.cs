﻿using ExamenEstebanP3.BOL.WSBCCR;
using ExamenEstebanP3.DAL;
using ExamenEstebanP3.ENTITIES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenEstebanP3.BOL
{
    public class AppBOL
    {
        /// <summary>
        /// pide los datos al bccr, los inserta a la base de datos, los trae de vuelta y los devueve
        /// </summary>
        /// <returns>datos</returns>
        public List<EBCCR.Datos_de_INGC011_CAT_INDICADORECONOMIC> CargarTipoCambio()
        {
            List<EBCCR.Datos_de_INGC011_CAT_INDICADORECONOMIC> tipos = new List<EBCCR.Datos_de_INGC011_CAT_INDICADORECONOMIC>();
            wsindicadoreseconomicosSoapClient ws = new wsindicadoreseconomicosSoapClient("wsindicadoreseconomicosSoap");

            Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy")+"/"+DateTime.Today.AddDays(-30).ToString("dd/MM/yyyy"));
            for (int i = 0; i < 30; i++)
            {
                //Console.WriteLine(DateTime.Today.AddDays(-i).ToString("dd/MM/yyyy"));
                string xml = ws.ObtenerIndicadoresEconomicosXML("318", DateTime.Today.AddDays(-i).ToString("dd/MM/yyyy"),
                DateTime.Today.AddDays(-i).ToString("dd/MM/yyyy"), "Esteban", "N", "emurillor@est.utn.ac.cr", "ABRREISNRN");
               // Console.WriteLine(xml);
                var datos=xml.ParseXML<EBCCR.Datos_de_INGC011_CAT_INDICADORECONOMIC>();
                //Console.WriteLine(datos.INGC011_CAT_INDICADORECONOMIC.NUM_VALOR);
                tipos.Add(datos);
            }

            new AppDAL().Insertar(tipos);
            //tipos = new AppDAL().TraerIndicadores();

            return tipos;
        }
    }
}
